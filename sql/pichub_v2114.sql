-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- ����: localhost
-- ����� ��������: ��� 19 2013 �., 20:43
-- ������ �������: 5.1.61-log
-- ������ PHP: 5.3.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES cp1251 */;

--
-- ���� ������: `pichubru`
--

-- --------------------------------------------------------

--
-- ��������� ������� `tbl_albums`
--

CREATE TABLE IF NOT EXISTS `tbl_albums` (
  `CODE` int(11) NOT NULL AUTO_INCREMENT,
  `NAZV` char(255) DEFAULT NULL,
  `USER` int(11) DEFAULT NULL,
  `ALL_IMG` int(11) DEFAULT '0',
  `DATEADD` int(11) DEFAULT NULL,
  PRIMARY KEY (`CODE`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=85 ;

-- --------------------------------------------------------

--
-- ��������� ������� `tbl_ban`
--

CREATE TABLE IF NOT EXISTS `tbl_ban` (
  `CODE` int(11) NOT NULL AUTO_INCREMENT,
  `IP` char(15) DEFAULT NULL,
  `WHOBAN` int(11) DEFAULT NULL,
  `REASON` text,
  `DATE` int(11) DEFAULT NULL,
  PRIMARY KEY (`CODE`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- ��������� ������� `tbl_conf`
--

CREATE TABLE IF NOT EXISTS `tbl_conf` (
  `CODE` int(11) NOT NULL AUTO_INCREMENT,
  `PARAM` char(100) NOT NULL DEFAULT '',
  `VALUESTR` char(200) DEFAULT NULL,
  `VALUEINT` int(11) DEFAULT NULL,
  PRIMARY KEY (`CODE`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=19 ;

-- --------------------------------------------------------

INSERT INTO `tbl_conf` (`CODE`, `PARAM`, `VALUESTR`, `VALUEINT`) VALUES
(1, 'IS_MAINTOP', NULL, 0),
(2, 'IS_MAINLAST', NULL, 0),
(3, 'IS_MAINRAND', NULL, 0),
(4, 'AUTODEL', NULL, 180),
(5, 'SIZE_LASTTOP', NULL, 75),
(6, 'MAIN_NUM_LASTTOP', NULL, 6),
(7, 'SIZE_RAND', NULL, 150),
(8, 'NUM_RAND', NULL, 10),
(9, 'MAX_IMG_SIZE', NULL, 5),
(10, 'NUM_LASTTOP', NULL, 100),
(11, 'PROFILE_IMG_PERPAGE', NULL, 20),
(12, 'IS_SNOW', NULL, 0),
(13, 'VERSION', '2.1.14', NULL),
(14, 'UT_SINGLE', NULL, 1),
(15, 'UT_MULTI', NULL, 1),
(16, 'UT_MULTIURL', NULL, 1),
(17, 'UT_WEBCAM', NULL, 1),
(18, 'VK_GROUP', 'http://vk.com/pichub', NULL);

--
-- ��������� ������� `tbl_del_request`
--

CREATE TABLE IF NOT EXISTS `tbl_del_request` (
  `CODE` int(11) NOT NULL AUTO_INCREMENT,
  `IMG` varchar(32) NOT NULL DEFAULT '',
  `OPIS` text,
  `USER` int(11) DEFAULT '0',
  `IP` char(15) DEFAULT NULL,
  `DATEADD` int(11) DEFAULT NULL,
  PRIMARY KEY (`CODE`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=151 ;

-- --------------------------------------------------------

--
-- ��������� ������� `tbl_feedback`
--

CREATE TABLE IF NOT EXISTS `tbl_feedback` (
  `CODE` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` char(50) DEFAULT NULL,
  `EMAIL` char(50) DEFAULT NULL,
  `THEME` char(100) DEFAULT NULL,
  `MESSAGE` text,
  `DATE` int(11) DEFAULT NULL,
  PRIMARY KEY (`CODE`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=16 ;

-- --------------------------------------------------------

--
-- ��������� ������� `tbl_img`
--

CREATE TABLE IF NOT EXISTS `tbl_img` (
  `CODE` varchar(32) NOT NULL,
  `FORMAT` varchar(4) DEFAULT NULL,
  `OPIS` text,
  `WHOADD` int(11) DEFAULT NULL,
  `IP` char(15) DEFAULT NULL,
  `VIEWS` int(11) DEFAULT '0',
  `ONAIR` int(1) DEFAULT '0',
  `EDITCODE` varchar(32) DEFAULT NULL,
  `ALBUM` int(11) DEFAULT '0',
  `GPSLat` varchar(11) DEFAULT NULL,
  `GPSLong` varchar(11) DEFAULT NULL,
  `File_datetime` int(11) DEFAULT NULL,
  `File_camera` char(100) DEFAULT NULL,
  `File_adress` char(100) DEFAULT NULL,
  `DATEADD` int(11) DEFAULT NULL,
  `LASTVIEW` int(11) DEFAULT NULL,
  PRIMARY KEY (`CODE`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- ��������� ������� `tbl_pages`
--

CREATE TABLE IF NOT EXISTS `tbl_pages` (
  `CODE` int(11) NOT NULL AUTO_INCREMENT,
  `NAZV` char(100) DEFAULT NULL,
  `TEXT` text,
  `DATEADD` int(11) DEFAULT NULL,
  PRIMARY KEY (`CODE`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- ���� ������ ������� `tbl_pages`
--

INSERT INTO `tbl_pages` (`CODE`, `NAZV`, `TEXT`, `DATEADD`) VALUES
(1, '� �������', '<p>\r\n\r\n<b>PicHub.ru</b> - ������������������ ������, ����������� ����� � ������ ��������� �������� � ���������� � ���������� ��� �� ������ �� ���������, �������������� ��� �������� ���������� �� �� �������, ������, �� � �.�.<br><br>\r\n<b>������������:</b><br>\r\n� �� ��������� �����������.<br>\r\n� ������ ��������� ���������.<br>\r\n� ����������� �������: <b>JPG, JPEG, GIF, PNG, BMP</b>. (<b>BMP</b> ������������� � <b>JPG</b>)<br>\r\n� ������������ ������ ����������� �� <b>5 ��</b>.<br>\r\n� ���� �������� <b>�� ���������</b>.<br>\r\n� �� ������ ���������� ���� �����������, ��������� ������������ ���������.<br><br><br>\r\n<center>������� ������ ����� <b>2.1.14</b></center><br>\r\n<div class="param-header" onclick="showhide(''ver_history'',2,''h'',200)" style="width:600px;">������� ������</div>\r\n<div class="param-body" id="ver_history" style="width:598px; display:none;">\r\n\r\n<ul style="text-align:left;">\r\n   <b>��������� � 2.1.14:</b>\r\n   <li>������� ��� �������� �����������</li>\r\n   <li>��������� ��� � ��������������� ������</li>\r\n   <li>������� �������������� � ��� ������ ���������� ���� ��� flash</li>\r\n   <li>��� javascript ������������ ������� �������� � ����� ����������� �����</li>\r\n   <li>javascript ������������ �� ��������������</li>\r\n   <li>��������� ���: �� ������������ ������ ��� ��������������</li>\r\n   <li>���� � ���������� ���� ���-�����, �� ��� ����� ����� ���������� �� ����� Google, ����� �� ��������������� ������ ��� ��������� �����������</li>\r\n</ul>\r\n\r\n<ul style="text-align:left;">\r\n   <b>��������� � 2.1.7:</b>\r\n   <li>IE ���: ���������� ���� ������� �������</li>\r\n   <li>IE ���: ��������� � �������� �� ���������� ����� �� ��������</li>\r\n   <li>���: ��������� � �������� ���������� ������ �� �������</li>\r\n   <li>���: � ��������� � �������� � ������� ������� ���������� ����� ����� ������ ������������ ��� ���� ���������</li>\r\n   <li>���: ���� ������ ����� ������������ ����� �� �������, �� ��������� ��� �������� �� �����������</li>\r\n   <li>����������� ������� ������ "�����", ������ ��� ������ ����������� ����������, �� �� ��������</li>\r\n   <li>������ �������� �������������� ���������� ������ �� ���������� ���� � "_" � ��� �������������</li>\r\n</ul>\r\n\r\n<ul style="text-align:left;">\r\n   <b>��������� � 2.1:</b>\r\n   <li>��������� � �������� �� �������� ���������, � ����������</li>\r\n   <li>IE ���: ����� �������������� �� ���������� ������ �� ����������� � ����� ������</li>\r\n   <li>����� �� ������ ����� �������������� ��������� ����� ��� � �� ��������</li>\r\n   <li>���: �� ���������� ������ ����� �������������� ����� ����� �����</li>\r\n   <li>IE ���: ����� ����� �������� � ����� ��������</li>\r\n   <li>IE ���: ����� ���� � ����������� ����������� � ������������� � ������� ��� ���������</li>\r\n   <li>IE ���: ��������� ������ ��������� �����</li>\r\n   <li>IE ���: ����� ��������� �������� ���������� �����, � �� ����� ������������</li>\r\n   <li>IE ���: �� �������� ���������� � ���-������</li>\r\n   <li>IE ���: ��� ��������� ����������� �� ������������ ������ ���-�����</li>\r\n   <li>IE ���: �� ���������� ������� �� ��������</li>\r\n   <li>IE ���: ������� ����� ��� ��������� �����������</li>\r\n   <li>IE ���: ���������� ���������� �������</li>\r\n   <li>IE ���: ����������� ������������� ������ ��������� �����</li>\r\n   <li>IE ���: � ������ � �������� �� ���������� �� ������</li>\r\n   <li>������������ ����� �������� ��� ���������� �������� (����� ��������������)</li>\r\n   <li>������ ������� ������������� � ������ ��������������</li>\r\n   <li>����������� ������������ ����������� �� 100px</li>\r\n</ul>\r\n\r\n<ul style="text-align:left;">\r\n   <b>��������� � 2.0:</b>\r\n   <li>��������� �������������� � ����������</li>\r\n   <li>��������� ������������� �������� �� URL �� ���������</li>\r\n   <li>����������� ��������� ���������� � ���-������</li>\r\n   <li>������������������ ������������ ����� ��������� ������� ��� ����� �����������</li>\r\n   <li>������ �����: �����������, ����������� � ��. ���������� � ��������� ����, � ��������� ���������� �������� �����������</li>\r\n   <li>� ������� ������������ ���������� ���� ����������� ������������� �� ���� �����������</li>\r\n   <li>��� �������� ����� �������� ������ ������</li>\r\n   <li>��� ����������� ������� �� �������� ����������� ����� ������ ����� � ����������� (captcha), ����� �������� ����� �� �����</li>\r\n   <li>����� ����� ��������� ������ �� ������ ����� ���������</li>\r\n   <li>����� ��������� �������� ������ ������ ����� �� �������� ��������� �� �������� � ������������� � ��������� ����������� ������, ����� ��� ������� ���� ������� ������ �� ����������� �����</li>\r\n   <li>������������� ������������� (radio) � ������ (checkbox)</li>\r\n   <li>����������� ��������/��������� ������������ ��� IP-�����</li>\r\n</ul>\r\n\r\n<ul style="text-align:left;">\r\n   <b>��������� � 1.03:</b>\r\n   <li>�������� �� ������� � ������ ����� ������� � ��� ������������</li>\r\n</ul>\r\n\r\n<ul style="text-align:left;">\r\n   <b>��������� � 1.02:</b>\r\n   <li>������� �������� "� �������", �� ������� ����� �������� ��� ������������ �����</li>\r\n   <li>�� ����� ������ ����</li>\r\n</ul>\r\n</div>\r\n</p>', 1324640983),
(2, '�������', '<p>1. ����������� ���������� �����������, ������� ��� ��� ���� �������� ������������ ����������������.</p>\r\n<p>2. ��� ���������� ����������� ������� ��������� � ������ ��������� �����. (��� ����������� ������ �� ��������� �����, ��� ����, ��� ����������, ����� �������)</p>\r\n<p>3. ����������� ������������� ������� ��� ���������� �����������, ���� �������, ����������� ������������������� ��������������, �����������, ���������� ����� �������� �� ������� �����, �����������, ���������� ������� ���� ������� ������������ ������� ���������� �������� � ������ ������.</p>\r\n<p>4. �� �������������� ���������� �� ���������������� �����������, ������������ ����������� ������������ ������� � ������ �������������.</p>\r\n<p>5. ����������� ���������, ���� � ������� 6 ������� � ���� ������������� ���������. ������ ��������� � ����� �� �����������. �����������, ���������� � ��������, �� ���������.</p>\r\n<p>6. ������ �� ������������ ��� ���������� �����������, ��������� ������� ��������� ������. (���������� �� ������������, �������� ��������� ���������� ������, ��������� ��������, �.�.�.).</p>\r\n<p>7. ������������� ������� ��������� �� ����� ����� ������� �����������, �� ��������������� ��������.</p>\r\n<p>8. ������������� ������� ����� ��������� ����� ����������� �� ��������� ������������� �����.</p>\r\n<p>9. � ������ ��������� ������, ����� ����������� ����������� � ���, ��� ��� ������� ������ ����� ���� �������� ������� ����� ��� ��������������.</p>', 1326897344);


--
-- ��������� ������� `tbl_referer`
--

CREATE TABLE IF NOT EXISTS `tbl_referer` (
  `CODE` int(11) NOT NULL AUTO_INCREMENT,
  `URL` char(255) DEFAULT NULL,
  `IP` char(15) DEFAULT NULL,
  `DATE` int(11) DEFAULT NULL,
  PRIMARY KEY (`CODE`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=48943 ;

-- --------------------------------------------------------

--
-- ��������� ������� `tbl_user`
--

CREATE TABLE IF NOT EXISTS `tbl_user` (
  `CODE` int(11) NOT NULL AUTO_INCREMENT,
  `LOGIN` char(50) NOT NULL DEFAULT '',
  `EMAIL` char(50) DEFAULT NULL,
  `PASS` char(100) DEFAULT NULL,
  `ALL_IMG` int(11) DEFAULT '0',
  `STATUS` int(1) DEFAULT '1',
  `FORGOT_KEY` char(50) DEFAULT NULL,
  `BAN` int(1) DEFAULT '0',
  `BAN_REASON` text,
  `DATEREG` int(11) DEFAULT NULL,
  PRIMARY KEY (`CODE`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=55 ;

--
-- ���� ������ ������� `tbl_user`
--

INSERT INTO `tbl_user` (`CODE`, `LOGIN`, `EMAIL`, `PASS`, `DATEREG`, `ALL_IMG`, `STATUS`, `FORGOT_KEY`) VALUES
(1, 'admin', 'admin@admin', '21232f297a57a5a743894a0e4a801fc3', '2011-10-20 18:11:54', 0, 2, NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
