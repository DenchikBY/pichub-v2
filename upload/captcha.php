<?php

session_start();
header("Content-Type: image/gif");

header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache");

$captcha = imagecreate(70, 20);
imagecolorallocate($captcha, 245, 245, 245);
$captcha_number = $_SESSION['captcha_number'];
imagestring($captcha, 5, 8, 2, $captcha_number, imagecolorallocate($captcha, 73, 126, 194));
imagegif($captcha);
imagedestroy($captcha);

?>