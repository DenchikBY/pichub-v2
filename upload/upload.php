<?php
set_time_limit(0);
require_once('config/functlist.php');
require_once('config/img.php');

$do = $_GET['do'];

if($do=='multi'){
   $_COOKIE['pichub_login'] = $_POST['pichub_login'];
   $_COOKIE['pichub_pass'] = $_POST['pichub_pass'];
}

verifyuser();

function dirforimg($type){
   $y = date("y");
   $m = date("m");
   $d = date("d");
   if(substr(sprintf('%o', fileperms("pic_".$type)), -4)!="0777"){
      chmod("pic_".$type, 0777);
   }
   if(!is_dir("pic_".$type."/".$y)){
      mkdir("pic_".$type."/".$y,0770);
   }
   if(!is_dir("pic_".$type."/".$y."/".$m)){
      mkdir("pic_".$type."/".$y."/".$m,0770);
   }
   if(!is_dir("pic_".$type."/".$y."/".$m."/".$d)){
      mkdir("pic_".$type."/".$y."/".$m."/".$d,0770);
   }
   return "pic_".$type."/".$y."/".$m."/".$d."/";
}

$files = array();

if(($do=='new' && $_FILES["Filedata"]["name"]!="") || $do=='multi' || $do=='edit' || $do=='webcam'){
   $files[] = '';
}elseif($do=='new' && $_FILES["Filedata"]["name"]=="" && $_POST['url_file']!=''){
   $files[] = $_POST['url_file'];
}elseif($do=='multiurl'){
   $files = $_POST['url_file'];
}else{
   header("Location: index.php");exit;
}

if($do=='multiurl'){
   $i = 0;
   $text = '';
}

foreach($files as $file){

   if($_FILES["Filedata"]['error']==UPLOAD_ERR_OK && ($do=='new' || $do=='multi')){
      $filename = $_FILES["Filedata"]["name"];
      $filepath = $_FILES["Filedata"]["tmp_name"];
      $filesize = filesize($filepath);
      $mb_filesize = round($filesize/(1024*1024),3);
      $type = exif_imagetype($filepath);
   }elseif(($_POST['url_file']!='' && $do=='new') || ($file!='' && $do=='multiurl')){
      if($do=='new'){
         $filename = $filepath = $_POST['url_file'];
      }elseif($do=='multiurl'){
         $filename = $filepath = $file;
      }
      if(!preg_match("~^(?:(?:https?|ftp|telnet)://(?:[a-z0-9_-]{1,32}"."(?::[a-z0-9_-]{1,32})?@)?)?(?:(?:[a-z0-9-]{1,128}\.)+(?:com|net|"."org|mil|edu|arpa|gov|biz|info|aero|inc|name|[a-z]{2})|(?!0)(?:(?"."!0[^.]|255)[0-9]{1,3}\.){3}(?!0|255)[0-9]{1,3})(:[0-9]{1,5})?(?:/[�-�a-z0-9.,_@%\(\)\*&amp;"."?+=\~/-]*)?(?:#[^ '\"&amp;<>]*)?$~i", $filename)){
         if($do=='new'){
            header("Location: index.php");exit;
         }elseif($do=='multiurl'){
            continue;
         }
      }
      $headers = get_headers($filepath);
      foreach($headers as $header){
         if(strpos($header, 'Content-Type:')!==false){
            $type = str_replace('Content-Type: image/', '', $header);
         }elseif(strpos($header, 'Content-Length: ')!==false){
            $filesize = str_replace('Content-Length: ', '', $header);
            $mb_filesize = round($filesize/(1024*1024),3);
         }
      }
      if($type=="jpeg"){
         $type = 2;
      }elseif($type=="gif"){
         $type = 1;
      }elseif($type=="png"){
         $type = 3;
      }elseif($type==""){
         $siteurl = substr($filename,0,strlen($domen));
         if($siteurl==$domen){
            $filename = $filepath = substr($filename,strlen($siteurl));
            $filesize = filesize($filepath);
            $mb_filesize = round($filesize/(1024*1024),3);
            $type = exif_imagetype($filepath);
         }else{
            if($do=='new'){
               header("Location: index.php");exit;
            }elseif($do=='multiurl'){
               continue;
            }
         }
      }
   }elseif($do=='edit'){
      $imgcode = $_POST['code'];
      if(recordcount("tbl_img where CODE='".$imgcode."'")>0){
         $editcode = $_GET['editcode'];
         $rs_2 = mysql_query("select * from tbl_img where CODE='".$imgcode."'",$conn);
         $rs = mysql_fetch_array($rs_2);
            if(($usercode!=0 && $rs['WHOADD']==$usercode) || $userstatus==2 || $editcode==$rs['EDITCODE']){
               $dirpath = getdirbydata($rs['DATEADD']);
               $imgpath = $dirpath.$imgcode.'.'.$rs['FORMAT'];
               $filename = 'pic_b/'.$imgpath;
               $filepath = 'pic_b/'.$imgpath;
               $type = exif_imagetype($filepath);
            }else{
               header("Location: index.php");exit;
            }
         mysql_free_result($rs_2);
      }else{
         header("Location: index.php");exit;
      }
   }elseif($do!='webcam'){
      if($do=='new'){
         header("Location: index.php");exit;
      }elseif($do=='multiurl'){
         continue;
      }
   }

   if($do!='webcam'){
      $ext = getext($filename);
      $max_filesize = getconf("MAX_IMG_SIZE","VALUEINT");
      if($max_filesize==""){ $max_filesize = 5; }
   }else{
      $ext = 'jpg';
   }

   if($do=='new' || $do=='multi'){
      $a = array("jpg","jpeg","JPG","JPEG","png","PNG","gif","GIF","bmp","BMP");
      $error = false;
      if ($_FILES["Filedata"]["name"]!="" && (!isset($_FILES['Filedata']) || !is_uploaded_file($filepath))){
         $error = 'Проблема при загрузке';
      }elseif ($filesize == 0){
         $error = 'Пустой файл';
      }elseif ($mb_filesize > $max_filesize){
         $error = 'Размер загружаемого файла не должен превышать '.$max_filesize.' Мб';
      }elseif (!in_array($ext,$a)){
         $error = 'Неверное расширение файла';
      }elseif (!($type==1 || $type==2 || $type==3 || $type==6)){
         $error = 'Неверный тип файла';
      }
   }

   if ($error && $do!='edit'){

      if($do=='new'){
         header("Location: index.php");exit;
      }elseif($do=='multi'){
         echo '{"status":0,"error":"'.$error.'"}';exit;
      }elseif($do=='multiurl'){
         continue;
      }

   }else{

      if($do=='new' || $do=='multi' || $do=='multiurl' || $do=='webcam'){
         $ps_folder = dirforimg("b");
         $imgcode = generatekey();
         while(recordcount("tbl_img where CODE='".$imgcode."'") > 0){
            $imgcode = generatekey();
         }
      }elseif($do=='edit'){
         $ps_folder = 'pic_b/'.$dirpath;
      }

      $ps_filename = $imgcode.'.'.$ext;

      $quality = $_POST['quality'];
      if($quality>100 || !preg_match("/^[0-9]+$/", $quality) || $quality==''){ $quality = 90; }

      if($_POST['check_animation']!=1 || $do=='edit'){

         if($type==2){
            $image = imagecreatefromjpeg($filepath);
         }elseif($type==1){
            $image = imagecreatefromgif($filepath);
         }elseif($type==3){
            $image = imagecreatefrompng($filepath);
         }elseif($type==6){
            $image = imagecreatefrombmp($filepath);

         }elseif($do=='webcam'){
            $input = file_get_contents('php://input');
            if(md5($input) == '7d4df9cc423720b7f1f3d672b89362be'){
               exit;
            }
            $image = imagecreatefromstring($input);

         }else{
            if($do=='new'){
               header("Location: index.php");exit;
            }elseif($do=='multiurl'){
               continue;
            }
         }

         if($do=='new' || $do=='edit'){

            $reflect = $_POST['reflect'];
            if($reflect==1 || $reflect==2){
               $image = reflect($image,$reflect);
            }

            if($_POST['orig_rotate']!=0 && ($_POST['orig_rotate']==90 || $_POST['orig_rotate']==180 || $_POST['orig_rotate']==270)){
               $image = imagerotate($image, $_POST['orig_rotate'], 0);
            }

            $resize = $_POST['resize'];
            if($resize>0){
               if($resize==1){
                  $value = $_POST['resize_w'];
               }elseif($resize==2){
                  $value = $_POST['resize_h'];
               }elseif($resize==3){
                  $value = $_POST['resize_p'];
               }
               $image = resize($image,$resize,$value);
            }

            $filter = $_POST['filter'];
            if($filter!=""){
               $image = filter($image,$filter,"");
            }

            $light_perc = $_POST['light'];
            if($light_perc>0 && $light_perc<=100){
               $image = filter($image,"light",$light_perc);
            }

            if($_POST['check_blur']==1){
               $image = filter($image,"blur","");
            }

            $contrast_perc = $_POST['contrast'];
            if($contrast_perc>0 && $contrast_perc<=100){
               $image = filter($image,"contrast",$contrast_perc);
            }

            $smooth_perc = $_POST['smooth'];
            if($smooth_perc>0 && $smooth_perc<=100){
               $image = filter($image,"smooth",$smooth_perc);
            }

            $title = iconv('windows-1251', 'utf-8', $_POST['title']);
            $title_color = $_POST['title-color'];
            $title_size = $_POST['title-size'];
            if($title!="" && ($title_color=="white" || $title_color=="black" || $title_color=="green" || $title_color=="red" || $title_color=="blue" || $title_color=="yellow") && $title_size>=8 && $title_size<=72){
               add_title($image,$title,$title_color,$title_size);
            }

            if($resize==0 && $reflect==0){
               imagesavealpha($image, true);
            }

            $corner_radius = $_POST['radius'];
            if($corner_radius>=3 && $corner_radius<=100){
               $image = corner_radius($image,$corner_radius);
            }

            if($_POST['reflection_effect']==1){
               $image = reflection_effect($image);
            }

         }

         if($type==2 || $type==6 || $_POST['check_jpg']==1 || $do=='webcam'){
            if($type==2 || $type==6 || $_POST['check_jpg']==1 || $ext=="jpeg" || $ext=="JPEG" || $ext=="bmp" || $ext=="BMP"){ if($do=='edit'){ unlink("pic_b/".$imgpath); unlink("pic_c/".$imgpath); unlink("pic_s/".$imgpath); } $ext = 'jpg'; $ps_filename = $imgcode.'.jpg'; }
            $ps1 = imagejpeg($image, $ps_folder.$ps_filename, $quality);
         }elseif($type==1){
            $ext = 'gif'; $ps_filename = $imgcode.'.gif';
            $ps1 = imagegif($image, $ps_folder.$ps_filename);
         }elseif($type==3){
            $ext = 'png'; $ps_filename = $imgcode.'.png';
            $ps1 = imagepng($image, $ps_folder.$ps_filename);
         }

      }else{
         $ps1 = copy($filepath,$ps_folder.$ps_filename);
      }

      if($do=='new' || $do=='multi' || $do=='multiurl' || $do=='webcam'){
         $s_dir = dirforimg("s");
         $c_dir = dirforimg("c");
      }elseif($do=='edit'){
         $s_dir = 'pic_s/'.$dirpath;
         $c_dir = 'pic_c/'.$dirpath;
      }

      $prev_size = $_POST['prev_size'];
      if(!ereg("([0-9])", $prev_size)){
         $prev_size = 180;
      }

      Thumbnail($ps_filename,'h',$prev_size,$ps_folder,$s_dir,$quality,$_POST['isprevsize']);
      crop($ps_filename,150,$ps_folder,$c_dir,$quality);

      if($ps1=="1" && ($do=='new' || $do=='multi' || $do=='multiurl' || $do=='webcam')){
         if($usercode==""){ $usercode = 0; }


         //echo '{"status":0,"error":"'.$_POST['album'].'"}';exit;


         $onair = $_POST['pr'];
         $editcode = generatekey();
         $opis = '';
         if($do=='new'){ $opis = $_POST['opis']; }
         $album = $_POST['album'];
         if($album!="" && ($album>0 && recordcount("tbl_albums where CODE=".$album." and USER=".$usercode)==0)){
            $album = 0;
         }

         if($type==2){
            $meta = exif_read_data($filepath, NULL, true, false);
            $file_camera = $meta[IFD0][Make].' '.$meta[IFD0][Model];
            $file_datetime = $meta[FILE][FileDateTime];
            $geo = getGeodataFromMeta($meta[GPS]);
            if($geo['lat']>0 && $geo['lon']>0){
               $json = json_decode(file_get_contents('http://maps.google.com/maps/api/geocode/json?latlng='.$geo['lat'].','.$geo['lon'].'&sensor=false&language=ru'));
               $status = $json->status;
               if($status == 'OK'){
                  $file_adress =  iconv('utf-8', 'windows-1251', $json->results[0]->formatted_address);
               }
            }
         }

         mysql_query("insert into tbl_img (CODE,FORMAT,OPIS,DATEADD,WHOADD,IP,ONAIR,EDITCODE,ALBUM,GPSLat,GPSLong,File_datetime,File_camera,File_adress) values ('".$imgcode."','".$ext."','".$opis."','".time()."','".$usercode."','".getIP()."','".$onair."','".$editcode."','".$album."','".$geo['lat']."','".$geo['lon']."','".$file_datetime."','".$file_camera."','".$file_adress."')",$conn);
         if($usercode>0){ mysql_query("update tbl_user set ALL_IMG=(ALL_IMG+1) where CODE='".$usercode."'",$conn); }
         if($album>0){ mysql_query("update tbl_albums set ALL_IMG=(ALL_IMG+1) where CODE='".$album."'",$conn); }

      }elseif($ps1=="1" && $do=='edit'){
         $opis = $_POST['opis'];
         mysql_query("update tbl_img set FORMAT='".$ext."',OPIS='".$opis."' where CODE='".$imgcode."'",$conn);
      }

      imagedestroy($image);

      if($do=='new' || $do=='edit'){
         header("Location: show/".$imgcode."/".$editcode);exit;
      }elseif($do=='multi' || $do=='webcam'){
         echo '{
          "status":1,
          "code":"'.$imgcode.'",
          "editcode":"'.$editcode.'",
          "dir":"'.$s_dir.'",
          "imgformat":"'.$ext.'"';
         if($do=="multi"){
            $filesize = round(filesize($ps_folder.$ps_filename)/1024,2); $size_pref = "kB";
	    if($filesize > 1024){ $filesize = round($filesize/1024,1); $size_pref = "MB"; }
            echo ',"filesize":"'.$filesize.' '.$size_pref.'"';
         }
         echo '}';
      }elseif($do=='multiurl'){
         $i++;
         list($width,$height) = getimagesize($ps_folder.$ps_filename);
         $text .= '<tr>';
         $text .= '<td width="200px"><a href="show/'.$imgcode.'/'.$editcode.'" target="_blank"><img src="'.$c_dir.$ps_filename.'"></a></td>';
         $text .= '<td width="400px"><a href="show/'.$imgcode.'/'.$editcode.'" target="_blank"><b>'.$imgcode.'</b></a><br><br>��� ��������������: <b>'.$editcode.'</b><br>������ �����������: '.$width.'x'.$height.'<br>������ �����: '.(round(filesize($ps_folder.$ps_filename)/1024,2)).' Kb<br>ip ��������: '.getIP().'</td>';
         $text .= '</tr><tr height="15px"></tr>';
      }

   }
}

if($do=='multiurl'){
   require_once('show_uploaded.php');
}

?>