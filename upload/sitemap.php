<?php

require_once('config/functlist.php');

$xml = '<?xml version="1.0" encoding="windows-1251" ?>';
$xml .= '<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">';

$xml .= '<url><loc>'.$domen.'index.php?single</loc><changefreq>weekly</changefreq><priority>1</priority></url>';
$xml .= '<url><loc>'.$domen.'index.php?multi</loc><changefreq>weekly</changefreq><priority>1</priority></url>';
$xml .= '<url><loc>'.$domen.'index.php?multiurl</loc><changefreq>weekly</changefreq><priority>1</priority></url>';
$xml .= '<url><loc>'.$domen.'index.php?webcam</loc><changefreq>weekly</changefreq><priority>1</priority></url>';

$xml .= '<url><loc>'.$domen.'?single</loc><changefreq>weekly</changefreq><priority>1</priority></url>';
$xml .= '<url><loc>'.$domen.'?multi</loc><changefreq>weekly</changefreq><priority>1</priority></url>';
$xml .= '<url><loc>'.$domen.'?multiurl</loc><changefreq>weekly</changefreq><priority>1</priority></url>';
$xml .= '<url><loc>'.$domen.'?webcam</loc><changefreq>weekly</changefreq><priority>1</priority></url>';

$xml .= '<url><loc>'.$domen.'rules</loc><changefreq>monthly</changefreq><priority>0.9</priority></url>';
$xml .= '<url><loc>'.$domen.'viewstop</loc><changefreq>daily</changefreq><priority>0.9</priority></url>';
$xml .= '<url><loc>'.$domen.'last</loc><changefreq>daily</changefreq><priority>0.9</priority></url>';
$xml .= '<url><loc>'.$domen.'contacts</loc><changefreq>monthly</changefreq><priority>0.9</priority></url>';
$xml .= '<url><loc>'.$domen.'about</loc><changefreq>weekly</changefreq><priority>0.9</priority></url>';

$rs_2 = mysql_query("select * from tbl_user order by DATEREG",$conn);
while (($rs=mysql_fetch_assoc($rs_2))!==false){
   $xml .= '<url><loc>'.$domen.'user/'.$rs['LOGIN'].'</loc><changefreq>weekly</changefreq><priority>0.8</priority></url>';
}
mysql_free_result($rs_2);

$rs_2 = mysql_query("select * from tbl_albums order by DATEADD",$conn);
while (($rs=mysql_fetch_assoc($rs_2))!==false){
   $xml .= '<url><loc>'.$domen.'album/'.$rs['CODE'].'</loc><changefreq>weekly</changefreq><priority>0.8</priority></url>';
}
mysql_free_result($rs_2);

$rs_2 = mysql_query("select * from tbl_img order by DATEADD",$conn);
while (($rs=mysql_fetch_assoc($rs_2))!==false){
   $imgpath = getdirbydata($rs['DATEADD']).$rs['CODE'].'.'.$rs['FORMAT'];
   $xml .= '<url><loc>'.$domen.'show/'.$rs['CODE'].'</loc><changefreq>weekly</changefreq><priority>0.8</priority><image:image><image:loc>'.$domen.'pic_b/'.$imgpath.'</image:loc></image:image><image:image><image:loc>'.$domen.'pic_s/'.$imgpath.'</image:loc></image:image><image:image><image:loc>'.$domen.'pic_c/'.$imgpath.'</image:loc></image:image></url>';
}
mysql_free_result($rs_2);


$xml .= '</urlset>';

header('Content-type: text/xml');

echo $xml;

?>