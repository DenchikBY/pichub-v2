<?php

require_once('lite.php');

class Cache_Lite_Output extends Cache_Lite {

    function Cache_Lite_Output($options)
    {
        $this->Cache_Lite($options);
    }

    function start($id, $group = 'default', $doNotTestCacheValidity = false)
    {
        $data = $this->get($id, $group, $doNotTestCacheValidity);
        if ($data !== false) {
            echo($data);
            return true;
        }
        ob_start();
        ob_implicit_flush(false);
        return false;
    }

    function end()
    {
        $data = ob_get_contents();
        ob_end_clean();
        $this->save($data, $this->_id, $this->_group);
        echo($data);
    }

}

?>