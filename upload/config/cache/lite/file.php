<?php
 
require_once('lite.php');

class Cache_Lite_File extends Cache_Lite{

    var $_masterFile = '';
    var $_masterFile_mtime = 0;
	
    function Cache_Lite_File($options = array(NULL))
    {   
        $options['lifetime'] = 0;
        $this->Cache_Lite($options);
        if (isset($options['masterFile'])) {
            $this->_masterFile = $options['masterFile'];
        } else {
            return $this->raiseError('Cache_Lite_File : masterFile option must be set !');
        }
        if (!($this->_masterFile_mtime = @filemtime($this->_masterFile))) {
            return $this->raiseError('Cache_Lite_File : Unable to read masterFile : '.$this->_masterFile, -3);
        }
    }

    function get($id, $group = 'default') 
    {
        if ($data = parent::get($id, $group, true)) {
            if ($filemtime = $this->lastModified()) {
                if ($filemtime > $this->_masterFile_mtime) {
                    return $data;
                }
            }
        }
        return false;
    }

}

?>