<?php

function imagecreatefrombmp($p_sFile){
   $file = fopen($p_sFile,"rb");
   $read = fread($file,10);
   while(!feof($file)&&($read<>"")){ $read .= fread($file,1024); }
   $temp = unpack("H*",$read);
   $hex = $temp[1];
   $header = substr($hex,0,108);
   if (substr($header,0,4)=="424d"){
      $header_parts = str_split($header,2);
      $width = hexdec($header_parts[19].$header_parts[18]);
      $height = hexdec($header_parts[23].$header_parts[22]);
      unset($header_parts);
   }
   $x = 0;
   $y = 1;
   $image = imagecreatetruecolor($width,$height);
   $body = substr($hex,108);
   $body_size  = strlen($body)/2;
   $header_size = $width*$height;
   $usePadding = $body_size>($header_size*3)+4;
   for ($i=0;$i<$body_size;$i+=3){
      if ($x>=$width){
         if ($usePadding){ $i += $width%4; }
         $x = 0;
         $y++;
         if ($y>$height){ break; }
      }
      $i_pos = $i*2;
      $r = hexdec($body[$i_pos+4].$body[$i_pos+5]);
      $g = hexdec($body[$i_pos+2].$body[$i_pos+3]);
      $b = hexdec($body[$i_pos].$body[$i_pos+1]);
      $color = imagecolorallocate($image,$r,$g,$b);
      imagesetpixel($image,$x,$height-$y,$color);
      $x++;
   }
   unset($body);
   return $image;
}

function Thumbnail($filename,$type,$value,$image_directory,$thumbs_directory,$quality,$size_title){
   $size_orig = filesize($image_directory.$filename);
   if ($size_orig > 0 ){
      $ext = getext($filename);
      if($ext=="jpg" || $ext=="JPG"){
         $im = imagecreatefromjpeg($image_directory.$filename);
      }elseif($ext=="gif" || $ext=="GIF"){
         $im = imagecreatefromgif($image_directory.$filename);
      }elseif($ext=="png" || $ext=="PNG"){
         $im = imagecreatefrompng($image_directory.$filename);
      }
      $ox = imagesx($im);
      $oy = imagesy($im);
      if($type=="h"){
         $nr = $oy;
      }elseif($type=="w"){
         $nr = $ox;
      }
      if($nr > $value){
         if($type=="h"){
            $ny = $value;
            $nx = floor($ox * ($value / $oy));
         }elseif($type=="w"){
            $nx = $value;
            $ny = floor($oy * ($value / $ox));
         }
         $nm = imagecreatetruecolor($nx, $ny);
         imagefill($nm, 0, 0, 0xFFFFFF);
         $white = imagecolorallocate($nm, 255, 255, 255);
         imagecolortransparent($nm, $white);
         imagecopyresampled($nm, $im, 0,0,0,0,$nx,$ny,$ox,$oy);
      }else{
         $nm = $im;
         $nx = $ox;
         $ny = $oy;
      }
      if($size_title==1){
         $src = imagecreatetruecolor($nx, 20);
         $color = imagecolorallocate($src, 255, 255, 255);
         $filesize = round($size_orig/1024,1); $sizef = 'Kb';
         if($filesize > 1024){ $filesize = round($filesize/1024,1); $sizef = 'Mb'; }
         imagettftext($src, 8, 0, 5, 14, $color, 'font.ttf',$ox.' x '.$oy.' ('.$filesize.' '.$sizef.')');
         imagecopymerge($nm, $src, 0, $ny-20, 0, 0, $ox, 20, 70);
         imagedestroy($src);
      }
      if($ext=="jpg" || $ext=="JPG"){
         imagejpeg($nm, $thumbs_directory.$filename, $quality);
      }elseif($ext=="gif" || $ext=="GIF"){
         imagegif($nm, $thumbs_directory.$filename);
      }elseif($ext=="png" || $ext=="PNG"){
         imagepng($nm, $thumbs_directory.$filename);
      }
      imagedestroy($im);
      imagedestroy($nm);
   }
}


function crop($filename,$size,$image_directory,$crop_directory,$quality){

   $ext = getext($filename);
   list($current_width, $current_height) = getimagesize($image_directory.$filename);

   if($current_height >= $size || $current_width >= $size){

      if($current_height > $current_width){
         $x1 = 0;
         $y1 = round(($current_height - $current_width)/2);
         $w = $current_width;
         $h = $current_width;
      }else if($current_width > $current_height){
         $x1 = round(($current_width - $current_height)/2);
         $y1 = 0;
         $w = $current_height;
         $h = $current_height;
      }else if($current_width == $current_height){
         $x1 = 0;
         $y1 = 0;
         $w = $current_width;
         $h = $current_height;
      }

      $crop_width = $size;
      $crop_height = $size;

      if($ext=="jpg" || $ext=="JPG"){
         $current_image = imagecreatefromjpeg($image_directory.$filename);
      }else if($ext=="gif" || $ext=="GIF"){
         $current_image = imagecreatefromgif($image_directory.$filename);
      }else if($ext=="png" || $ext=="PNG"){
         $current_image = imagecreatefrompng($image_directory.$filename);
      }

      $new = imagecreatetruecolor($crop_width, $crop_height);
      imagefill($new, 0, 0, 0xFFFFFF);
      $white = imagecolorallocate($new, 255, 255, 255);
      imagecolortransparent($new, $white);

      imagecopyresampled($new, $current_image, 0, 0, $x1, $y1, $crop_width, $crop_height, $w, $h);

      if($ext=="jpg" || $ext=="JPG"){
         imagejpeg($new, $crop_directory.$filename, $quality);
      }else if($ext=="gif" || $ext=="GIF"){
         imagegif($new, $crop_directory.$filename);
      }else if($ext=="png" || $ext=="PNG"){
         imagepng($new, $crop_directory.$filename);
      }

      imagedestroy($current_image);
      imagedestroy($new);

   }else{
      copy($image_directory.$filename, $crop_directory.$filename);
   }
}

function resize($image,$type,$value){
   $cur_height = imagesy($image);
   $cur_width = imagesx($image);
   if($type==1){
      $width = $value;
      if($width<$cur_width){ $height = round($width/$cur_width*$cur_height); }
   }elseif($type==2){
      $height = $value;
      if($height<$cur_height){ $width = round($height/$cur_height*$cur_width); }
   }elseif($type==3){
      $percents = $value;
      if($percents<100){ 
         $height = round($cur_height/100*$percents);
         $width = round($cur_width/100*$percents);
      }
   }
   if($width!="" && $height!=""){
      $im = imagecreatetruecolor($width,$height);
      imagefill($im, 0, 0, 0xFFFFFF);
      $white = imagecolorallocate($im, 255, 255, 255);
      imagecolortransparent($im, $white);
      imagecopyresampled($im,$image,0,0,0,0,$width,$height,$cur_width,$cur_height);
      imagedestroy($image);
   }else{
      $im = $image;
   }
   return $im;
}

function reflect($image,$type){
   $cur_height = imagesy($image);
   $cur_width = imagesx($image);
   $temp = imagecreatetruecolor($cur_width, $cur_height);
   imagefill($temp, 0, 0, 0xFFFFFF);
   $white = imagecolorallocate($temp, 255, 255, 255);
   imagecolortransparent($temp, $white);
   if($type==1){
      imagecopyresampled($temp, $image, 0, 0, $cur_width-1, 0, $cur_width, $cur_height, 0-$cur_width, $cur_height);
   }elseif($type==2){
      imagecopyresampled($temp, $image, 0, 0, 0, $cur_height-1, $cur_width, $cur_height, $cur_width, 0-$cur_height);
   }
   imagedestroy($image);
   return $temp;
}

function reflection_effect($src_img){

   $src_height = imagesy($src_img);
   $src_width = imagesx($src_img);
   $dest_height = $src_height + $src_height*0.4;
   $dest_width = $src_width;

   $reflected = imagecreatetruecolor($dest_width, $dest_height);
   imagefill($reflected, 0, 0, 0xFFFFFF);
   $white = imagecolorallocate($reflected, 255, 255, 255);
   imagecolortransparent($reflected, $white);

   imagecopy($reflected, $src_img, 0, 0, 0, 0, $src_width, $src_height);
   $reflection_height = $src_height*0.4;
   $alpha_step = 100 / $reflection_height;
   for ($y = 1; $y <= $reflection_height; $y++){
      for ($x = 0; $x < $dest_width; $x++){
         $rgba = imagecolorat($src_img, $x, $src_height - $y);
         $alpha = ($rgba & 0x7F000000) >> 24;
         $alpha = max($alpha, 7 + ($y * $alpha_step));
         $rgba = imagecolorsforindex($src_img, $rgba);
         $rgba = imagecolorallocatealpha($reflected, $rgba['red'], $rgba['green'], $rgba['blue'], $alpha);
         imagesetpixel($reflected, $x, $src_height + $y - 1, $rgba);
      }
   }

   return $reflected;
}

function add_title($image,$text,$color,$size){
   $cur_height = imagesy($image);
   $cur_width = imagesx($image);

   if($color=="white"){
      $color = imagecolorallocate($image, 255, 255, 255);
   }elseif($color=="black"){
      $color = imagecolorallocate($image, 0, 0, 0);
   }elseif($color=="green"){
      $color = imagecolorallocate($image, 0, 128, 0);
   }elseif($color=="red"){
      $color = imagecolorallocate($image, 255, 0, 0);
   }elseif($color=="blue"){
      $color = imagecolorallocate($image, 0, 0, 255);
   }elseif($color=="yellow"){
      $color = imagecolorallocate($image, 255, 255, 14);
   }

   imagettftext($image, $size, 0, 10, $cur_height-($cur_height*0.03), $color, 'font.ttf',$text);
}

function filter($img,$filter,$value){

   if($filter=="grey"){
      imagefilter($img, IMG_FILTER_GRAYSCALE);
   }elseif($filter=="red"){
      imagefilter($img, IMG_FILTER_COLORIZE, 255, 0, 0);
   }elseif($filter=="green"){
      imagefilter($img, IMG_FILTER_COLORIZE, 0, 255, 0);
   }elseif($filter=="blue"){
      imagefilter($img, IMG_FILTER_COLORIZE, 0, 0, 255);
   }elseif($filter=="negate"){
      imagefilter($img, IMG_FILTER_NEGATE);
   }elseif($filter=="light"){
      imagefilter($img, IMG_FILTER_BRIGHTNESS, $value);
   }elseif($filter=="blur"){
      imagefilter($img, IMG_FILTER_GAUSSIAN_BLUR);
   }elseif($filter=="contrast"){
      imagefilter($img, IMG_FILTER_CONTRAST, $value);
   }elseif($filter=="smooth"){
      imagefilter($img, IMG_FILTER_SMOOTH, $value);
   }elseif($filter=="sepia"){
      imagefilter($img,IMG_FILTER_GRAYSCALE);
      imagefilter($img,IMG_FILTER_BRIGHTNESS,-30);
      imagefilter($img,IMG_FILTER_COLORIZE, 90, 55, 30);
   }

   imagesavealpha($img, true);
   return $img;
}

function corner_radius($img,$radius){
   $rate = 5;
   $width = imagesx($img);
   $height = imagesy($img);
   imagealphablending($img, false);
   imagesavealpha($img,true);
   $rs_radius = $radius *$rate;
   $rs_size = $rs_radius *2;
   $corner = imagecreatetruecolor($rs_size, $rs_size);
   imagefill($corner, 0, 0, 0xFFFFFF);
   $trans = imagecolorallocate($corner, 255, 255, 255);
   imagecolortransparent($corner, $trans);
   $positions = array(
      array(0, 0, 0, 0),
      array($rs_radius, 0,$width-$radius, 0),
      array($rs_radius,$rs_radius, $width-$radius, $height-$radius),
      array(0, $rs_radius, 0,$height-$radius),
   );
   foreach ($positions as $pos){
      imagecopyresampled($corner, $img, $pos[0],$pos[1], $pos[2], $pos[3], $rs_radius,$rs_radius, $radius, $radius);
   }
   $lx = $ly = 0;
   $i =-$rs_radius;
   $y2 =-$i;
   $r_2 = $rs_radius *$rs_radius;
   for (; $i <= $y2; $i++){
      $y = $i;
      $x = sqrt($r_2-$y * $y);
      $y += $rs_radius;
      $x += $rs_radius;
      imageline($corner, $x,$y, $rs_size, $y, $trans);
      imageline($corner, 0,$y, $rs_size-$x, $y,$trans);
      $lx = $x;
      $ly = $y;
   }
   foreach ($positions as $i => $pos){
      imagecopyresampled($img, $corner, $pos[2],$pos[3], $pos[0], $pos[1], $radius, $radius,$rs_radius, $rs_radius);
   }
   imagedestroy($corner);
   return $img;
}

function fractionResult($fraction){
   list($sup, $sub) = explode('/', $fraction);
   if(floatval($sub) != 0){
      return $sup / $sub;
   }else{
      return 0;
   }
}

function getCoord($fractional, $ref){
   $coord = 0;
   if(is_array($fractional)){
      $coord = fractionResult($fractional[0]) + fractionResult($fractional[1]) / 60 + fractionResult($fractional[2]) / 3600;
   }
   if($ref == "S" || $ref == "W"){
      $coord = -$coord;
   }
   return $coord;
}

function getGeodataFromMeta($meta){
   if(isset($meta['GPSLatitude'])){
      $lat = getCoord($meta['GPSLatitude'], $meta['GPSLatitudeRef']);
      $long = getCoord($meta['GPSLongitude'], $meta['GPSLongitudeRef']);
      $geo = array(
         'lat' => $lat,
         'lon' => $long,
      );
   }
   return $geo;
}

?>