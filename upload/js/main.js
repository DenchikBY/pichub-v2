﻿jQuery.noConflict();
IE='\v'=='v';

function gid(id){
   return document.getElementById(id);
}

function setCookie(name, value, expires, path, domain, secure){
   document.cookie = name + "=" + escape(value) +
      ((expires) ? "; expires=" + expires : "") +
      ((path) ? "; path=" + path : "") +
      ((domain) ? "; domain=" + domain : "") +
      ((secure) ? "; secure" : "");
}

function str_replace(search, replace, subject){
   return subject.split(search).join(replace);
}

function checkIfFlashEnabled(){
   var isFlashEnabled = false;
   if (typeof(navigator.plugins)!="undefined" && typeof(navigator.plugins["Shockwave Flash"])=="object"){
      isFlashEnabled = true;
   } else if (typeof  window.ActiveXObject != "undefined"){
      try{
         if (new ActiveXObject("ShockwaveFlash.ShockwaveFlash")){
            isFlashEnabled = true;
         }
      } catch(e){}
   }
   return isFlashEnabled;
}

var isflash = checkIfFlashEnabled();

function onselectimg($id){
   filename = gid($id).value;
   arr = filename.split('.');
   ext = arr[arr.length-1];
   $id==="file" && ($true_url=true);
   if($id==="url_file"){
      var checkURL = /^(?:(?:https?|ftp|telnet):\/\/(?:[a-zа-я0-9_-]{1,32}(?::[a-zа-я0-9_-]{1,32})?@)?)?(?:(?:[a-zа-я0-9-]{1,128}\.)+(?:com|net|org|mil|edu|arpa|ru|gov|biz|info|aero|inc|name|[a-zа-я]{2})|(?!0)(?:(?!0[^.]|255)[0-9]{1,3}\.){3}(?!0|255)[0-9]{1,3})(?:\/[a-zа-я0-9.,_@%&?+=\~\/-]*)?(?:#[^ \'\"&<>]*)?$/i;
      $true_url = checkURL.test(filename);
   }
   if((ext==="jpg" || ext==="JPG" || ext==="jpeg" || ext==="JPEG" || ext==="gif" || ext==="GIF" || ext==="png" || ext==="PNG" || ext=="bmp" || ext=="BMP") && $true_url===true){
      jQuery('#imgset').animate({ height: 'show' });
   }else{
      gid('imgset').style.display = 'none';
      if($true_url===false){
         $msg = 'Неверная ссылка';
      }else{
         document.getElementById("file_div").innerHTML = document.getElementById("file_div").innerHTML;
         $msg = 'Выбран файл неверного расширения';
      }
      alert($msg);
   }
}

function modal_form(header,url,width){
   if(header!=="" && url!==""){
      if(gid('blockMsg')){
         gid('blockMsg').innerHTML = '<img src="'+domen+'img/loading.gif">';
      }else{
         jQuery.blockUI({message: '<img src="'+domen+'img/loading.gif">' , css: { left:'45%', top:'50%', width:'200px' }});
      }
      jQuery.get(domen+url, function(data){
         if(data!==""){
            text = '<div class="modalform_header"><div class="modalform_header_nazv">'+header+'</div><div class="modalform_header_close" onclick="jQuery.unblockUI();">x</div></div><div id="form-body" class="modalform_body">'+data+'</div>';
            window_width = jQuery(window).width();
            if(width>0 && width<window_width){
               left = (window_width-width)/2;
            }else{
               width = '30%';
               left = '35%';
            }
            jQuery.blockUI({ message: text , css: { width:width, left:left, top:'20%', boxShadow: '0 0 5px white' } });
            window_height = window.innerHeight;
            form_height = gid('blockMsg').clientHeight;
            gid('blockMsg').style.top = Math.round((window_height-form_height)/2);
         }else{
            jQuery.unblockUI();
         }
      });
   }
}

function delimg($code,$editcode){
   if($code!==""){
      window.location.href = domen+"showimage.php?do=del&img="+$code+"&editcode="+$editcode;
   }
}

function delrequest(){
   dr_reason = gid('reason').value;
   dr_captcha = gid('captcha').value;
   if(dr_reason===""){
      gid('error').innerHTML = '<img src="../img/error.gif"> Введите причину';
      return false;
   }else{
      reason_length = dr_reason.length;
      if(reason_length<20){
         gid('error').innerHTML = '<img src="../img/error.gif"> Минимум 20 символов';
         return false;
      }else{
         gid('error').innerHTML = '';
         captcha_length = dr_captcha.length;
         if(dr_captcha==="" || captcha_length===0 || captcha_length<6){
            gid('error_captcha').innerHTML = '<img src="../img/error.gif"> Введите цифры с картинки';
            return false;
         }else{
            gid('error_captcha').innerHTML = '';
            code_arr = window.location.href.split('show/');
            code = code_arr[1];
            jQuery.post('../showimage.php?img='+code+'&do=dr', {reason:dr_reason,captcha:dr_captcha}, function(data){
               if(data==='01'){
                  gid('error').innerHTML = '<img src="../img/error.gif"> Введите причину';
                  jQuery('#captcha_img').replaceWith('<img src="'+domen+'captcha.php" id="captcha_img">');
                  return false;
               }else{
                  if(data==='02'){
                     gid('error').innerHTML = '';
                     gid('error_captcha').innerHTML = '<img src="../img/error.gif"> Цифры введены неверно';
                     jQuery('#captcha_img').replaceWith('<img src="'+domen+'captcha.php" id="captcha_img">');
                     return false;
                  }else{
                     jQuery('#del_request_div').replaceWith('');
                     gid('form-body').innerHTML = 'Запрос успешно отправлен администратору';
                     setTimeout('jQuery.unblockUI()', 2000);
                  }
               }
            });
         }
      }
   }
}

function check_auth(){
   $login = document.auth.login.value;
   $pass = document.auth.password.value;
   jQuery.get('login.php?do=check_login&login='+$login, function(data){
      if(data=="true"){
         gid('login_result').innerHTML = '<img src="img/error.gif"> Неверный логин'; return false;
      }else{
         gid('login_result').innerHTML = '<img src="img/noerror.gif">';
         jQuery.get('login.php?do=check_auth&login='+$login+'&pass='+$pass, function(data1){
            data1=="0" ? gid("pass_result").innerHTML='<img src="img/error.gif"> Неверный пароль' : document.auth.submit();
         });
      }
   });
}

function showhide(block,type,st,time){
   if(type===1){
      if (gid(block).style.display !== "none"){
         gid(block).style.display = "none";
      }else{
         gid(block).style.display = "";
      }
   }
   if(type===2){
      if (st==="hw" || st===""){ $st11 = "hide"; $st12 = "hide"; $st21 = "show"; $st22 = "show"; }
      if (st==="h"){ $st11 = "hide"; $st12 = "none"; $st21 = "show"; $st22 = "none"; }
      if (st==="w"){ $st11 = "none"; $st12 = "hide"; $st21 = "none"; $st22 = "show"; }
      if (gid(block).style.display !== "none"){
         jQuery('#'+block).animate({height:$st11, width:$st12}, time);
      }else{
         jQuery('#'+block).animate({height:$st21, width:$st22}, time);
      }
   }
}

function change_uptype(tab){
   if(isflash===true){
      var arr = jQuery.parseJSON('{"single":"0","multi":"1","multiurl":"2","webcam":"3"}');
   }else if(isflash===false){
      var arr = jQuery.parseJSON('{"single":"0","multiurl":"1"}');
   }
   if(!IE){ window.history.pushState("", "", '?'+tab); }
   jQuery('#uptype_single,#uptype_multi,#uptype_multiurl,#uptype_webcam').css('display','none');
   if(gid('uptype_'+tab).innerHTML === ''){
      jQuery('#uploadform').block({message: '<img src="img/loading.gif">', css: { marginTop: '40px'}});
      jQuery.get('uploadform_'+tab+'.php', function(data){
         jQuery('#uptype_'+tab).html(data);
      });
   }
   jQuery('#uptype_'+tab).css('display','');
   jQuery('#uptype_tabs a').removeClass('tab2_sel').addClass('tab2_unsel');
   jQuery('#uptype_tabs a:eq('+arr[tab]+')').removeClass('tab2_unsel').addClass('tab2_sel');
   jQuery('#uploadform').unblock();
}

function addurl($value){
   if($value===undefined){ $value = 1; }
   $text = '';
   for(i=0; i<$value; i++){
      $text += '<input type="text" name="url_file[]" class="span8" style="margin:1px;"><br>';
   }
   jQuery('#addurl').before($text);
}

function renamealbum($code){
   $name = gid("newalbumname").value;
   if($name.length===0){
      gid('error').innerHTML = '<img src="../img/error.gif"> Введите название альбома';
   }else{
      jQuery.post('../album.php?code='+$code+'&do=rename', {name:$name} , function(data){
         if(data==="1"){
            gid('form-body').innerHTML = 'Название альбома изменено на <span style="color:red;">'+$name+'</span>';
         }else{
            gid('form-body').innerHTML = 'Название альбома НЕ изменено';
         }
         setTimeout('window.location.href = "'+domen+'album/'+$code+'"', 2000);
      });
   }
}

function delalbum(code){
   type = jQuery('input[name="deltype"]:checked').val();
   new_album = gid('album').value;
   if(code>0 && (type==="1" || type==="2" || type==="3")){
      jQuery.post('../album.php?do=del&code='+code, {type:type,album:new_album} , function(data){
         info = jQuery.parseJSON(data);
         gid('form-body').innerHTML = info['message'];
         if(info['status']==="1"){
            setTimeout('window.location.href = "../user/'+info['user']+'"', 3000);
         }
      });
   }
}

function album_over(num){
   gid('ald1_'+num).style.border='#3b5998 1px solid';
   gid('ald2_'+num).style.border='#3b5998 1px solid';
}

function album_out(num){
   gid('ald1_'+num).style.border='#cccccc 1px solid';
   gid('ald2_'+num).style.border='#cccccc 1px solid';
}

function referermsg(url){
   jQuery('body').append('<div class="infomsg" id="referermsg" style="display:none;" onclick="close_infomsg(jQuery(this));" oncontextmenu="close_infomsg(jQuery(this));return false;"><img src="'+domen+'img/referer.png" style="float:left; width:70px; margin-right:5px; margin-top:5px;">Вы занесли нам в базу переход с сайта <a href="'+url+'" target="_blank">'+url+'</a></div>');
   jQuery('#referermsg').animate({height:"show"},500);
}

function close_infomsg(obj){
   jQuery(obj).fadeOut('slow');
}

function change_album(code){
   album = gid('album').value;
   jQuery.post('../showimage.php?img='+code+'&do=change_album', {album:album}, function(data){
      info = jQuery.parseJSON(data);
      gid('change_album_result').innerHTML = info['message'];
   });
}

function send_feedback_form(){
   $name = document.feedback.contactname.value;
   $mail = document.feedback.email.value;
   $theme = document.feedback.subject.value;
   $msg = document.feedback.message.value;
   error = 0;
   if($name===""){
      gid('name_result').innerHTML = '<img src="img/error.gif"> Введите имя';
      error = 1;
   }else{
      gid('name_result').innerHTML = '<img src="img/noerror.gif">';
   }
   reg = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
   if (!$mail.match(reg)){
      gid('mail_result').innerHTML = '<img src="img/error.gif"> Неверный email';
      error = 1;
   }else{
      gid('mail_result').innerHTML = '<img src="img/noerror.gif">';
   }
   if($theme===""){
      gid('theme_result').innerHTML = '<img src="img/error.gif"> Введите тему';
      error = 1;
   }else{
      gid('theme_result').innerHTML = '<img src="img/noerror.gif">';
   }
   if($msg===""){
      gid('msg_result').innerHTML = '<img src="img/error.gif"> Введите текст сообщения';
      error = 1;
   }else{
      if($msg.length<20){
         gid('msg_result').innerHTML = '<img src="img/error.gif"> Минимум 20 символов';
         error = 1;
      }else{
         gid('msg_result').innerHTML = '<img src="img/noerror.gif">';
      }
   }
   if(error===0){
      jQuery.post('contacts.php?do=send', {contactname:$name,email:$mail,subject:$theme,message:$msg} , function(data){
         if(data==="1"){
            document.feedback.contactname.value = '';
            gid('name_result').innerHTML = '';
            document.feedback.email.value = '';
            gid('mail_result').innerHTML = '';
            document.feedback.subject.value = '';
            gid('theme_result').innerHTML = '';
            document.feedback.message.value = '';
            gid('msg_result').innerHTML = '';
            jQuery("#feedback").before('<div class="msg" style="width:250;">Сообщение успешно отправлено!</div><br>');
         }else{
            jQuery("#feedback").before('<div class="msg" style="width:250;">Сообщение не отправлено!</div><br>');
         }
         setTimeout('jQuery(".msg").animate({ height:"hide" })', 3000);
      });
   }
}

function check_mail($value){
   reg = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
   if (!$value.match(reg)){
      gid('result_email').innerHTML = '<img src="img/error.gif"> Неверный email';
      return false;
   }else{
      jQuery.get('login.php?do=check_mail&mail='+$value, function(data){
         if(data=="false"){
            gid('result_email').innerHTML = '<img src="img/error.gif"> Такой email уже есть';
            return false;
         }
         if(data=="true"){
            gid('result_email').innerHTML = '<img src="img/noerror.gif">';
         }
      });
   }
   return true;
}

function check_login($value){
   $value = str_replace('.', '', $value);
   gid('login').value = $value;
   $length = $value.length;
   if($length < 4){
      gid('result_login').innerHTML = '<img src="img/error.gif"> Хотя бы 4 символа';
      return false;
   }else{
      jQuery.get('login.php?do=check_login&login='+$value, function(data){
         if(data=="false"){
            gid('result_login').innerHTML = '<img src="img/error.gif"> Такой логин уже есть';
            return false;
         }
         if(data=="true"){
            gid('result_login').innerHTML = '<img src="img/noerror.gif">';
         }
      });
   }
   return true;
}

function check_pass($value,$num){
   if($num===1){
      $length = $value.length;
      if($length < 5){
         gid('result_pass').innerHTML = '<img src="img/error.gif"> Хотя бы 5 символов';
         return false;
      }else{
         gid('result_pass').innerHTML = '<img src="img/noerror.gif">';
         $pass2 = gid('password2').value;
         if($pass2!==""){ check_pass($pass2,2); }
         return true;
      }
   }
   if($num===2){
      $pass = gid('password').value;
      if($pass===""){ gid('result_pass2').innerHTML = '<img src="img/error.gif"> Вы не ввели пароль'; return false; }
      if($pass!==$value){ gid('result_pass2').innerHTML = '<img src="img/error.gif"> Пароли не совпадают'; return false; }
      if($pass===$value){ gid('result_pass2').innerHTML = '<img src="img/noerror.gif">'; return true; }
   }
}

function check_form(){
   $mail = gid('email').value;
   $login = gid('login').value;
   $pass = gid('password').value;
   $pass2 = gid('password2').value;
   $c1 = check_mail($mail);
   $c2 = check_login($login);
   $c3 = check_pass($pass,1);
   $c4 = check_pass($pass2,2);
   if($c1===true && $c2===true && $c3===true && $c4===true){ gid('regform').submit(); }
}

function showuserimages($user,$part){
   jQuery("#moreimg").replaceWith('<div id="moreimg" style="cursor:default;"><b>Загрузка...</b></div>');
   jQuery.post(domen+'user.php?do=showuserimages', {user:$user,part:$part} , function(data){
      jQuery("#moreimg").replaceWith(data);
   });
}

function showuseralbums($user,$part){
   jQuery("#moreal").replaceWith('<div id="moreal" style="cursor:default;"><b>Загрузка...</b></div>');
   jQuery.post('../user.php?do=showuseralbums', {user:$user,part:$part} , function(data){
      jQuery("#moreal").replaceWith(data);
   });
}

function change_pass($login){
   $pass = gid("cur_pass").value;
   $new1_pass = gid("new1_pass").value;
   $new2_pass = gid("new2_pass").value;
   $error = 0;
   if($pass===""){
      gid('cur_pass_result').innerHTML = '<img src="../img/error.gif"> Введите текущий пароль';
      $error = 1;
   }else{
      jQuery.get('../login.php?do=check_auth&login='+$login+'&pass='+$pass, function(data){
         if(data!=='1'){
            gid('cur_pass_result').innerHTML = '<img src="../img/error.gif"> Неверный пароль';
            $error = 1;
         }
         if(data==='1'){
            gid('cur_pass_result').innerHTML = '<img src="../img/noerror.gif">';
         }
      });
   }
   if($new1_pass===""){
      gid('new1_pass_result').innerHTML = '<img src="../img/error.gif"> Введите новый пароль';
      $error = 1;
   }else{
      if($new1_pass.length<5){
         gid('new1_pass_result').innerHTML = '<img src="../img/error.gif"> Минимум 5 символов';
         $error = 1;
      }else{
         gid('new1_pass_result').innerHTML = '<img src="../img/noerror.gif">';
      }
   }
   if($new2_pass===""){
      gid('new2_pass_result').innerHTML = '<img src="../img/error.gif"> Введите повторно новый пароль';
      $error = 1;
   }else{
      if($new1_pass===""){
         gid('new2_pass_result').innerHTML = '<img src="../img/error.gif"> Введите новый пароль в поле выше';
         $error = 1;
      }
      if($new1_pass!==""){
         if($new1_pass!==$new2_pass){
            gid('new2_pass_result').innerHTML = '<img src="../img/error.gif"> Пароли не совпадают';
            $error = 1;
         }else{
            gid('new2_pass_result').innerHTML = '<img src="../img/noerror.gif">';
         }
      }
   }
   if($error===0){
      jQuery('#form-body').block({message: '<img src=../img/loading.gif>'});
      jQuery.post('../user.php?do=change_pass', {login:$login,pass:$pass,new1_pass:$new1_pass,new2_pass:$new2_pass} , function(data){
         if(data==="1"){
            gid('form-body').innerHTML = 'Пароль успешно изменен';
         }else{
            gid('form-body').innerHTML = 'Пароль не изменен, попробуйте еще раз';
         }
         setTimeout('jQuery.unblockUI()', 2000);
      });
   }
}

function pics_tab($tab,$user){
   if(gid('div_'+$tab).innerHTML === ''){
      jQuery('#tabs').block({message: null});
      jQuery.post(domen+'user.php?do=showuser'+$tab, {user:$user,part:1} , function(data){
         gid('div_'+$tab).innerHTML = data;
      });
   }
   if($tab === 'images'){
      gid('tab_albums').className = "summary_tab tab_unsel";
      gid('div_albums').style.display = 'none';
   }else{
      gid('tab_images').className = "summary_tab tab_unsel";
      gid('div_images').style.display = 'none';
   }
   gid('tab_'+$tab).className = "summary_tab tab_sel";
   gid('div_'+$tab).style.display = '';
   jQuery('#tabs').unblock();
}

function deluser(){
   $login_arr = window.location.href.split('user/');
   $login = $login_arr[1];
   $type = jQuery('input[name="deltype"]:checked').val();
   if($login!=="" && $type!==""){
      jQuery.post('../user.php?do=deluser', {login:$login,type:$type} , function(data){
         if(data==="success"){
            gid('form-body').innerHTML = 'Пользователь успешно удален';
            setTimeout('window.location.href = "../index.php"', 2000);
         }else{
            gid('form-body').innerHTML = 'Ошибка при удалении';
         }
      });
   }
}

function newalbum(){
   $name = gid("newalbumname").value;
   if($name.length===0){
      gid('error').innerHTML = '<img src="../img/error.gif"> Введите название альбома';
   }else{
      jQuery.post(domen+'user.php?do=newalbum', {name:$name} , function(data){
         gid('form-body').innerHTML = 'Альбом <span style="color:red;">'+$name+'</span> создан';
         setTimeout('window.location.href = "'+domen+'user/'+data+'?albums"', 2000);
      });
   }
}

function banuser(){
   login_arr = window.location.href.split('user/');
   login = login_arr[1];
   reason = gid('ban_reason').value;
   jQuery.post('../user.php?do=banuser&login='+login, {reason:reason} , function(data){
      info = jQuery.parseJSON(data);
      gid('form-body').innerHTML = info['message'];
      if(info['status']==='1'){
         setTimeout('window.location.href = "'+domen+'user/'+login+'"', 2000);
      }
   });
}

function unbanuser(){
   login_arr = window.location.href.split('user/');
   login = login_arr[1];
   jQuery.get('../user.php?do=unbanuser&login='+login, function(data){
      info = jQuery.parseJSON(data);
      gid('form-body').innerHTML = info['message'];
      if(info['status']==='1'){
         setTimeout('window.location.href = "'+domen+'user/'+login+'"', 2000);
      }
   });
}

function geo_pos(lat,lon,dt,cam,adress){
   modal_form('Карта','showimage.php?do=geo_pos&lat='+lat+'&lon='+lon+'&dt='+dt+'&cam='+cam+'&adress='+adress,800);
}

jQuery(document).ready(function($){
   if(isflash===false){
      jQuery('a[href="?multi"],a[href="?webcam"],#uptype_multi,#uptype_webcam').remove();
   }
   $('#vk').append('<span class="hover"/>').each(function(){
      var $span = $('> span.hover', this).css('opacity', 0);
      $(this).hover(function (){
         $span.stop().fadeTo(800, 1);
      }, function(){
         $span.stop().fadeTo(800, 0);
      });
   });
   $('#msg_ie').animate({top:"0"}, 800);
   $('.message').click(function(){
      $(this).animate({top: -$(this).outerHeight()}, 500);
      setCookie("msg_ie", "0");
   });
   if(gid('randimg_carousel')){
      $('#randimg_carousel').jcarousel({
         scroll: 1,
         auto: 2,
         wrap: "circular"
      });
   }
});

jQuery(window).scroll(function(){
   if(IE){
      scrolltop = jQuery(window).scrollTop();
      jQuery("#gentime_div").css("bottom",-scrolltop+30);
      jQuery("#gentime_clock").css("bottom",-scrolltop);
   }
});