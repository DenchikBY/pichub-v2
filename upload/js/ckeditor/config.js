﻿CKEDITOR.editorConfig = function( config ){

config.toolbar =
[
    [ 'Source' ],
    [ 'Undo','Redo','-','Find','SelectAll','Preview' ],
    [ 'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock' ],
    [ 'Bold','Italic','Underline','Strike','-','Subscript','Superscript','-','RemoveFormat' ],
    [ 'NumberedList','BulletedList' ],
    [ 'Outdent','Indent' ],
    [ 'BidiLtr','BidiRtl' ],
    "/",
    [ 'Font','FontSize' ],
    [ 'TextColor','BGColor' ],
    [ 'Image','Table','HorizontalRule','SpecialChar' ],
    [ 'Link','Unlink','Anchor' ],
    [ 'Maximize', 'ShowBlocks' ]
];

};