//window.addEvent('domready', function(){
	var up = new FancyUpload2($('multi-status'), $('multi-list'), {
		url: $('form-multi').action,
		path: 'js/fancyupload/Swiff.Uploader.swf',
		typeFilter: {
			'\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u044f (*.jpg, *.jpeg, *.gif, *.png, *.bmp)': '*.jpg; *.jpeg; *.gif; *.png; *.bmp'
		},
		target: 'multi-browse',
		fieldName: 'Filedata',

		appendCookieData: true,

		onLoad: function(){
			$('multi-status').removeClass('hide');
			$('multi-fallback').destroy();
			this.target.addEvents({
				click: function() {
					return false;
				},
				mouseenter: function() {
					this.addClass('hover');
				},
				mouseleave: function() {
					this.removeClass('hover');
					this.blur();
				},
				mousedown: function() {
					this.focus();
				}
			});
			$('multi-clear').addEvent('click', function(){
				up.remove();
				return false;
			});
			$('multi-upload').addEvent('click', function(){
				up.start();
				return false;
			});
		},
		onSelectFail: function(files) {
			files.each(function(file) {
				new Element('li', {
					'class': 'validation-error',
					html: file.validationErrorMessage || file.validationError,
					title: MooTools.lang.get('FancyUpload', 'removeTitle'),
					events: {
						click: function() {
							this.destroy();
						}
					}
				}).inject(this.list, 'top');
			}, this);
		},

		onFileSuccess: function(file, response) {
			var json = new Hash(JSON.decode(response, true) || {});
			if (json.get('status') == '1'){
				file.element.className = 'file';
				imgcode = json.get('code');
				imgeditcode = json.get('editcode');
				c_dir = json.get('dir');
				imgformat = json.get('imgformat');
				filesize = json.get('filesize');
				file.element.setStyles({
					'backgroundImage': 'url("'+c_dir+imgcode+'.'+imgformat+'")',
					'backgroundRepeat': 'no-repeat',
					'backgroundSize': '40px'
				});
				file.element.getElement('.file-name').set('html', '<br><b><a href="show/'+imgcode+'/'+imgeditcode+'" target="_blank">' + imgcode + '</a></b><br><br>');
				file.element.getElement('.file-size').set('html', filesize);
			}else{
				file.element.addClass('file-failed');
				file.info.set('html', '<strong>\u041e\u0448\u0438\u0431\u043a\u0430:</strong> ' + json.get('error'));
			}
		},
		onFail: function(error) {
			switch (error) {
				case 'hidden':
					alert('To enable the embedded uploader, unblock it in your browser and refresh (see Adblock).');
					break;
				case 'blocked':
					alert('To enable the embedded uploader, enable the blocked Flash movie (see Flashblock).');
					break;
				case 'empty':
					alert('A required file was not found, please be patient and we fix this.');
					break;
				case 'flash':
					alert('To enable the embedded uploader, install the latest Adobe Flash plugin.');
					break;
			}
		}
	});
//});