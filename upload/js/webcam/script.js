﻿jQuery(document).ready(function($){
	
	var camera = $('#camera'),
	    screen = $('#screen');

	/*----------------------------------
		Установки веб камеры
	----------------------------------*/


	webcam.set_swf_url('js/webcam/webcam.swf');
	webcam.set_api_url('upload.php?do=webcam');	// Скрипт загрузки
	webcam.set_quality(90);				// Качество фотографий JPEG
	webcam.set_shutter_sound(true, 'js/webcam/shutter.mp3');

	// Генерируем код HTML для камеры и добавляем его на страницу:	
	screen.html(
		webcam.get_html(screen.width(), screen.height())
	);


	/*------------------------------
		Обработчики событий
	-------------------------------*/


	var shootEnabled = false;
	if(IE){ shootEnabled = true; }
		
	$('#shootButton').click(function(){
		
		if(!shootEnabled){
			return false;
		}
		
		webcam.freeze();
		togglePane();
		return false;
	});
	
	$('#cancelButton').click(function(){
		webcam.reset();
		togglePane();
		return false;
	});
	
	$('#uploadButton').click(function(){
		webcam.upload();
		webcam.reset();
		togglePane();
		return false;
	});

	camera.find('.settings').click(function(){
		if(!shootEnabled){
			return false;
		}
		
		webcam.configure('camera');
	});


	/*---------------------- 
		Возвратные вызовы
	----------------------*/
	
	
	webcam.set_hook('onLoad',function(){
		// Когда FLASH загружен, разрешаем доступ 
		// к кнопкам "Снимаю" и "Установка"
		shootEnabled = true;
	});
	
	webcam.set_hook('onComplete', function(msg){
		$('#uploadform').unblock();
		msg = $.parseJSON(msg);
		if(msg.status===1){
			data = '<center><a href="'+domen+'show/'+msg.code+'/'+msg.editcode+'" target="_blank"><img src="'+domen+msg.dir+msg.code+'.'+msg.imgformat+'"></a><br><br>Код редактирования: <b>'+msg.editcode+'</b></center>';
			text = '<div class="modalform_header"><div class="modalform_header_nazv">Просмотр изображения</div><div class="modalform_header_close" onclick="jQuery.unblockUI();">x</div></div><div id="form-body" class="modalform_body">'+data+'</div>';
			jQuery.blockUI({ message: text , css: { left:'35%', top:'30%', boxShadow: '0 0 5px white' } });
		}else{
			text = '<div class="modalform_header"><div class="modalform_header_nazv">Ошибка</div><div class="modalform_header_close" onclick="jQuery.unblockUI();">x</div></div><div id="form-body" class="modalform_body">При загрузке возникла какая-то ошибка</div>';
			jQuery.blockUI({ message: text , css: { left:'35%', top:'30%', boxShadow: '0 0 5px white' } });
		}
	});
	
	webcam.set_hook('onError',function(e){
		screen.html(e);
	});
	

	/*----------------------------
		Вспомогательные функции
	------------------------------*/


	// Данная функция разрешает доступ к двум 
	// элментам div .buttonPane:
	
	function togglePane(){
		var visible = $('#camera .buttonPane:visible:first');
		var hidden = $('#camera .buttonPane:hidden:first');
		
		visible.fadeOut('fast',function(){
			hidden.show();
		});
	}
});